import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';

class CloseButtonWidget extends StatelessWidget {
  const CloseButtonWidget({
    required this.onTap,
    super.key,
  });

  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(Spacing.spacingXS),
        child: IconWidget.sizeM(
          color: iconSecondaryColors.secondary100,
          type: IconType.close,
        ),
      ),
    );
  }
}
