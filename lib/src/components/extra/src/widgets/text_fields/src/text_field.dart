import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';

class TextField extends StatelessWidget {
  const TextField({
    super.key,
    this.controller,
    this.initialValue,
    this.focusNode,
    this.decoration,
    this.maxLines,
    this.minLines,
    this.expands = false,
    this.maxLength,
    this.onChanged,
    this.onTap,
    this.onTapOutside,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.hintText,
    this.isError = false,
    this.isBorder = false,
  });

  final TextEditingController? controller;
  final String? initialValue;
  final FocusNode? focusNode;
  final InputDecoration? decoration;
  final int? maxLines;
  final int? minLines;
  final bool expands;
  final bool isError;
  final bool isBorder;
  final int? maxLength;
  final ValueChanged<String>? onChanged;
  final GestureTapCallback? onTap;
  final TapRegionCallback? onTapOutside;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onFieldSubmitted;

  final String? hintText;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final textFieldFilled = theme.extension<TextFieldFilledColors>()!;
    final textFieldEmptyColors = theme.extension<TextFieldEmptyColors>()!;
    final textSystem = theme.extension<TextSystemColors>()!;

    final transparentBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: Colors.transparent,
        width: 0.0,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final border = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final enabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final disabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.disabledBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final focusedBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final errorBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.badBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    return DecoratedBox(
      decoration: BoxDecoration(
        color: textFieldEmptyColors.defaultFill,
        border: Border.all(color: textFieldFilled.activatedBorder),
        borderRadius: const BorderRadius.all(
          Radius.circular(
            Radiuses.radiusXS,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(
          Spacing.spacingXS,
        ),
        child: TextFormField(
          controller: controller,
          initialValue: initialValue,
          focusNode: focusNode,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(
              0.0,
            ),
            hintText: hintText,
            hintMaxLines: 3,
            hintStyle: theme.textTheme.bodyMedium?.copyWith(
              color: textFieldEmptyColors.activatedText,
            ),
            labelStyle: theme.textTheme.bodyMedium?.copyWith(
              color: textFieldEmptyColors.activatedText,
            ),
            errorStyle: theme.textTheme.bodySmall?.copyWith(
              color: textSystem.bad,
            ),
            errorBorder: errorBorder,
            focusedBorder: isError
                ? errorBorder
                : isBorder
                    ? focusedBorder
                    : transparentBorder,
            focusedErrorBorder: errorBorder,
            disabledBorder: isError
                ? errorBorder
                : isBorder
                    ? disabledBorder
                    : transparentBorder,
            enabledBorder: isError ? errorBorder : transparentBorder,
            border: isError
                ? errorBorder
                : isBorder
                    ? border
                    : transparentBorder,
            fillColor: textFieldFilled.activatedFill,
            enabled: true,
            filled: true,
          ),
          maxLines: maxLines,
          minLines: minLines,
          expands: expands,
          maxLength: maxLength,
          onChanged: onChanged,
          onTap: onTap,
          onTapOutside: onTapOutside,
          onEditingComplete: onEditingComplete,
          onFieldSubmitted: onFieldSubmitted,
        ),
      ),
    );
  }
}
