part of '../index.dart';

class PersonalTrainerAppBar extends SliverPersistentHeaderDelegate {
  PersonalTrainerAppBar({
    required this.label,
    required this.imagePath,
    required this.expandedHeight,
    required this.tabBarLabelList,
    this.isPersonal = false,
    this.onBack,
    this.onSave,
    this.onAddMedia,
    this.imageLoadType = ImageLoadType.network,
  });

  final List<String> tabBarLabelList;
  final double expandedHeight;
  final String label;
  final String imagePath;
  final bool isPersonal;
  final GestureTapCallback? onBack;
  final GestureTapCallback? onSave;
  final GestureTapCallback? onAddMedia;
  final ImageLoadType imageLoadType;

  @override
  Widget build(
    BuildContext context,
    double shrinkOffset,
    bool overlapsContent,
  ) {
    final double imageSize;

    final calcImageSize = expandedHeight / 1.7 - shrinkOffset;

    if (calcImageSize < Sizing.sizeXL) {
      imageSize = Sizing.sizeXL;
    } else {
      imageSize = calcImageSize;
    }

    final k = (expandedHeight - shrinkOffset) / (expandedHeight);

    const maxLeftImage = Spacing.spacingM;

    final double imageLeft =
        (((MediaQuery.of(context).size.width - maxLeftImage * 2) / 2 - imageSize / 2) * k) + maxLeftImage;

    const maxTopImage = Sizing.sizeXM;
    const minTopImage = Sizing.size3XS;
    const deltaTopImage = maxTopImage - minTopImage;

    final topImage = minTopImage + deltaTopImage * k;

    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final textPrimaryColors = theme.extension<TextPrimaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;
    final surfaceColors = theme.extension<SurfaceColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;

    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        DecoratedBox(
          decoration: BoxDecoration(
            color: surfaceColors.white,
            border: Border(
              bottom: BorderSide(
                color: dividerSecondaryColors.secondary50,
              ),
            ),
          ),
        ),
        Positioned(
          top: topImage,
          left: imageLeft,
          child: ImageLoad.round(
            fit: BoxFit.cover,
            path: imagePath,
            radius: imageSize,
            imageLoadType: imageLoadType,
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: tabBarHeight + Spacing.spacingS,
          child: SizedBox(
            height: Sizing.sizeS,
            child: Center(
              child: Text(
                label,
                style: textTheme.displaySmall!.copyWith(
                  color: textSecondaryColors.secondary80,
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 0,
          top: 0,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: Spacing.spacingS,
              vertical: Spacing.spacingS + Spacing.spacing3XS,
            ),
            child: GestureDetector(
              onTap: onBack,
              child: IconWidget.sizeS(
                color: iconSecondaryColors.secondary70,
                type: IconType.arrowBack,
              ),
            ),
          ),
        ),
        if (isPersonal)
          Positioned(
            right: 0,
            top: 0,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: Spacing.spacingS,
                vertical: Spacing.spacingS + Spacing.spacing3XS,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  GestureDetector(
                    onTap: onSave,
                    child: IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.addAPhoto,
                    ),
                  ),
                  const SizedBox(
                    width: Spacing.spacing2XS,
                  ),
                  GestureDetector(
                    onTap: onSave,
                    child: IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.save,
                    ),
                  ),
                ],
              ),
            ),
          ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: SizedBox(
            height: Sizing.sizeXL,
            child: TabBar(
              /*labelColor: textPrimaryColors.primary80,
             */
              labelStyle: textTheme.bodyLarge!.copyWith(
                color: textSecondaryColors.secondary100,
              ),
              tabs: List<Tab>.generate(
                tabBarLabelList.length,
                (index) {
                  return Tab(
                    text: tabBarLabelList[index],
                  );
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight + tabBarHeight;

  double get tabBarHeight => Sizing.sizeXL;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
