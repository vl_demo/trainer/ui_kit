part of '../index.dart';

class RegisterAppBar extends AAppBar {
  const RegisterAppBar({
    super.onBack,
    this.onAddTap,
    required this.title,
    super.key,
  });

  final GestureTapCallback? onAddTap;
  final String title;

  @override
  State<RegisterAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<RegisterAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return Row(
      children: [
        GestureDetector(
          onTap: widget.onBack,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.arrowBack,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        const SizedBox(
          width: Sizing.sizeS,
        ),
      ],
    );
  }
}
