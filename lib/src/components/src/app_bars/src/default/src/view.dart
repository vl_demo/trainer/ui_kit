part of '../index.dart';

class DefaultAppBar extends AAppBar {
  const DefaultAppBar({
    super.onBack,
    required this.title,
    super.key,
  });
  final String title;

  @override
  State<DefaultAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<DefaultAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    return Row(
      children: [
        const SizedBox(
          height: Sizing.sizeS,
          width: Sizing.sizeS,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        const SizedBox(
          height: Sizing.sizeS,
          width: Sizing.sizeS,
        ),
      ],
    );
  }
}
