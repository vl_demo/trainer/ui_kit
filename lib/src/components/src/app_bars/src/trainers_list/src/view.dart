part of '../index.dart';

class TrainersListAppBar extends AAppBar {
  const TrainersListAppBar({
    this.onPersonTap,
    required this.title,
    super.key,
  });

  final GestureTapCallback? onPersonTap;
  final String title;

  @override
  State<TrainersListAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<TrainersListAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    final theme = Theme.of(context);

    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    return Row(
      children: [
        const SizedBox(
          width: Sizing.sizeS,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TitleAppBar(text: widget.title),
            ],
          ),
        ),
        GestureDetector(
          onTap: widget.onPersonTap,
          child: IconWidget.sizeS(
            color: iconSecondaryColors.secondary70,
            type: IconType.person,
          ),
        ),
      ],
    );
  }
}
