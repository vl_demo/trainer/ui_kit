part of '../index.dart';

abstract class AAppBar extends StatefulWidget implements PreferredSizeWidget {
  const AAppBar({
    super.key,
    this.onBack,
  });

  static const double _kNavBarPersistentHeight = 55.0;

  final GestureTapCallback? onBack;

  @override
  State<AAppBar> createState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(_kNavBarPersistentHeight);
  }
}

abstract class AAPState<T extends StatefulWidget> extends State<T> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final surfaceColors = theme.extension<SurfaceColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    final resultConstruction = buildContent(context);

    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: dividerSecondaryColors.secondary30,
          ),
        ),
      ),
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                color: surfaceColors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(
                  Spacing.spacingS,
                  Spacing.spacingXL,
                  Spacing.spacingS,
                  Spacing.spacing2XS,
                ),
                child: resultConstruction,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context);

  Widget subtitle({required String text}) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }

  Widget printingSubtitle({required String text}) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
        //height: 1.0,
      ),
    );
  }

  ImageLoad getImage({
    required String path,
    double? width,
    double? height,
    fit = BoxFit.contain,
    imageLoadType = ImageLoadType.asset,
  }) =>
      ImageLoad(
        path: path,
        width: width,
        height: height,
        fit: fit,
        imageLoadType: imageLoadType,
      );
}

class TitleAppBar extends StatelessWidget {
  const TitleAppBar({
    required this.text,
    super.key,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      overflow: TextOverflow.ellipsis,
      style: textTheme.headlineLarge?.copyWith(
        color: textSecondaryColors.secondary100,
      ),
    );
  }
}

class APMemberCountChatAppBar extends StatelessWidget {
  const APMemberCountChatAppBar({
    required this.text,
    required this.count,
    super.key,
  });

  final String text;
  final int count;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      '$count $text',
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }
}

class APSubTitleChatAppBar extends StatelessWidget {
  const APSubTitleChatAppBar({
    required this.text,
    super.key,
  });

  final String text;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Text(
      text,
      style: textTheme.bodySmall?.copyWith(
        color: textSecondaryColors.secondary70,
      ),
    );
  }
}
