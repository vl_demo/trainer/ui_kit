part of '../index.dart';

class ImageItemWidget extends StatelessWidget {
  const ImageItemWidget({
    super.key,
    required this.imagePath,
    this.imageLoadType = ImageLoadType.network,
  });

  final String imagePath;
  final ImageLoadType imageLoadType;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return DecoratedBox(
      decoration: BoxDecoration(
        color: backgroundColors.gray,
        borderRadius: const BorderRadius.all(
          Radius.circular(
            Radiuses.radius2XS,
          ),
        ),
        border: Border.all(
          color: dividerSecondaryColors.secondary30,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(
          Spacing.spacing2XS,
        ),
        child: ImageLoad(
          fit: BoxFit.cover,
          path: imagePath,
          imageLoadType: imageLoadType,
        ),
      ),
    );
  }
}
