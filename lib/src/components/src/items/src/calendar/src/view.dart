part of '../index.dart';

class CalendarItemWidget extends StatelessWidget {
  const CalendarItemWidget({
    super.key,
    required this.value,
    required this.label,
    this.onTap,
  });

  final String label;
  final String value;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;
    final dividerPrimaryColors = theme.extension<DividerPrimaryColors>()!;

    final Color textColor = textSecondaryColors.secondary100;
    final Color backgroundColor = backgroundColors.gray;

    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(
          Spacing.spacing2XS,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              label,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: textTheme.headlineLarge!.copyWith(
                color: textColor,
              ),
            ),
            const SizedBox(
              height: Spacing.spacing2XS,
            ),
            DecoratedBox(
              decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: const BorderRadius.all(
                  Radius.circular(
                    Radiuses.radius2XS,
                  ),
                ),
                border: Border.all(
                  color: dividerPrimaryColors.primary80,
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(
                  Spacing.spacingXS,
                ),
                child: Row(
                  children: [
                    Text(
                      value,
                      style: textTheme.displaySmall!.copyWith(
                        color: textColor,
                      ),
                    ),
                    const Spacer(),
                    IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.calendarMonth,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
