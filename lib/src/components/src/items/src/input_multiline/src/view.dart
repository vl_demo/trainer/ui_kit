part of '../index.dart';

class InputMultilineItemWidget extends StatelessWidget {
  InputMultilineItemWidget({
    super.key,
    required this.label,
    this.isSecure = false,
  }) : textEditingController = TextEditingController();

  const InputMultilineItemWidget.controller({
    super.key,
    required this.label,
    this.isSecure = false,
    required this.textEditingController,
  });

  final String label;
  final bool isSecure;

  final TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Padding(
      padding: const EdgeInsets.all(
        Spacing.spacing2XS,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: textTheme.headlineLarge!.copyWith(
              color: textSecondaryColors.secondary100,
            ),
          ),
          const SizedBox(
            height: Spacing.spacing2XS,
          ),
          ui_kit.TextField(
            controller: textEditingController,
          ),
        ],
      ),
    );
  }
}
