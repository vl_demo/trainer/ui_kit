part of '../index.dart';

enum ScheduleSubItemState {
  fill,
  empty,
}

class ScheduleSubItemWidget extends StatelessWidget {
  const ScheduleSubItemWidget({
    super.key,
    required this.startTime,
    required this.endTime,
    required this.label,
    this.onStartTap,
    this.onEndTap,
    this.onCloseTap,
    this.isEdit = false,
  }) : state = ScheduleSubItemState.fill;

  const ScheduleSubItemWidget.empty({
    super.key,
    required this.label,
  })  : onStartTap = null,
        onEndTap = null,
        onCloseTap = null,
        state = ScheduleSubItemState.empty,
        startTime = null,
        endTime = null,
        isEdit = false;

  final String label;
  final String? startTime;
  final String? endTime;
  final ScheduleSubItemState state;
  final GestureTapCallback? onStartTap;
  final GestureTapCallback? onEndTap;
  final GestureTapCallback? onCloseTap;
  final bool isEdit;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;

    final Color textColor = textSecondaryColors.secondary100;

    final List<Widget> body;

    switch (state) {
      case ScheduleSubItemState.empty:
        body = [const Spacer()];
        break;
      case ScheduleSubItemState.fill:
        body = [
          if (startTime == null)
            const SizedBox(
              width: Spacing.spacing4XL,
            ),
          if (startTime != null)
            SizedBox(
              width: Spacing.spacing4XL,
              child: Center(
                child: Text(
                  '$startTime',
                  style: textTheme.headlineLarge!.copyWith(
                    color: textColor,
                  ),
                ),
              ),
            ),
          GestureDetector(
            onTap: onStartTap,
            child: IconWidget.sizeS(
              color: iconSecondaryColors.secondary70,
              type: IconType.schedule,
            ),
          ),
          if (endTime == null)
            const SizedBox(
              width: Spacing.spacing4XL,
            ),
          if (endTime != null)
            SizedBox(
              width: Spacing.spacing4XL,
              child: Center(
                child: Text(
                  '$endTime',
                  style: textTheme.headlineLarge!.copyWith(
                    color: textColor,
                  ),
                ),
              ),
            ),
          GestureDetector(
            onTap: onEndTap,
            child: IconWidget.sizeS(
              color: iconSecondaryColors.secondary70,
              type: IconType.schedule,
            ),
          )
        ];
        break;
    }

    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: Spacing.spacing3XS,
      ),
      child: Row(
        children: [
          SizedBox(
            width: Sizing.size2XL,
            child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                '$label:',
                style: textTheme.headlineLarge!.copyWith(
                  color: textColor,
                ),
              ),
            ),
          ),
          ...body,
          if (isEdit) ...[
            const Spacer(),
            GestureDetector(
              onTap: onCloseTap,
              child: IconWidget.sizeS(
                color: iconSecondaryColors.secondary70,
                type: IconType.close,
              ),
            ),
          ]
        ],
      ),
    );
  }
}
