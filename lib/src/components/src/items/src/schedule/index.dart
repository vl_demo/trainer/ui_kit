import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';

import 'extra/schedule_item/index.dart';
import 'src/model.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/view.dart';
