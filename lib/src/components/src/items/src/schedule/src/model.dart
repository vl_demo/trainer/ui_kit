import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

enum TimeScheduleType {
  start,
  end,
}

enum WeekdayType {
  sun,
  mon,
  tue,
  wed,
  thu,
  fri,
  sat,
}

@freezed
class ScheduleSubItemModel with _$ScheduleSubItemModel {
  const factory ScheduleSubItemModel({
    TimeOfDay? startDateTime,
    TimeOfDay? endDateTime,
  }) = _ScheduleSubItemModel;
}

@freezed
class ScheduleItemModel with _$ScheduleItemModel {
  const factory ScheduleItemModel({
    required Map<WeekdayType, ScheduleSubItemModel> map,
  }) = _ScheduleItemModel;
}
