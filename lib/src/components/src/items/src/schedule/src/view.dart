part of '../index.dart';

class ScheduleItemWidget extends StatelessWidget {
  ScheduleItemWidget({
    super.key,
    required ScheduleItemModel model,
    this.onScheduleTap,
    this.isEdit = false,
    required this.label,
    required this.sunLabel,
    required this.monLabel,
    required this.tueLabel,
    required this.wedLabel,
    required this.thuLabel,
    required this.friLabel,
    required this.satLabel,
  }) : controller = ScheduleItemController(model: model);

  const ScheduleItemWidget.controller({
    super.key,
    required this.controller,
    this.onScheduleTap,
    this.isEdit = false,
    required this.label,
    required this.sunLabel,
    required this.monLabel,
    required this.tueLabel,
    required this.wedLabel,
    required this.thuLabel,
    required this.friLabel,
    required this.satLabel,
  });

  final ScheduleItemController controller;
  final String label;
  final String sunLabel;
  final String monLabel;
  final String tueLabel;
  final String wedLabel;
  final String thuLabel;
  final String friLabel;
  final String satLabel;
  final void Function(
    TimeScheduleType timeScheduleType,
    WeekdayType weekdayType,
  )? onScheduleTap;
  final bool isEdit;

  String _weekdayToString(WeekdayType v) {
    switch (v) {
      case WeekdayType.sun:
        return sunLabel;
      case WeekdayType.mon:
        return monLabel;
      case WeekdayType.tue:
        return tueLabel;
      case WeekdayType.wed:
        return wedLabel;
      case WeekdayType.thu:
        return thuLabel;
      case WeekdayType.fri:
        return friLabel;
      case WeekdayType.sat:
        return satLabel;
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerPrimaryColors = theme.extension<DividerPrimaryColors>()!;

    return Padding(
      padding: const EdgeInsets.all(
        Spacing.spacing2XS,
      ),
      child: ValueListenableBuilder<ScheduleItemModel>(
        valueListenable: controller,
        builder: (
          BuildContext context,
          ScheduleItemModel value,
          Widget? child,
        ) {
          final List<ScheduleSubItemWidget> children = [];

          if (isEdit) {
            for (final key in WeekdayType.values) {
              if (value.map.keys.contains(key)) {
                final item = value.map[key];

                children.add(
                  ScheduleSubItemWidget(
                    isEdit: true,
                    startTime: item?.startDateTime == null ? null : item!.startDateTime!.format(context),
                    endTime: item?.endDateTime == null ? null : item!.endDateTime!.format(context),
                    label: _weekdayToString(key),
                    onStartTap: () {
                      if (onScheduleTap == null) {
                        return;
                      }

                      onScheduleTap!(TimeScheduleType.start, key);
                    },
                    onEndTap: () {
                      if (onScheduleTap == null) {
                        return;
                      }

                      onScheduleTap!(TimeScheduleType.end, key);
                    },
                    onCloseTap: () {
                      controller.removeItem(key);
                    },
                  ),
                );
              } else {
                children.add(
                  ScheduleSubItemWidget(
                    isEdit: true,
                    label: _weekdayToString(key),
                    onStartTap: () {
                      if (onScheduleTap == null) {
                        return;
                      }

                      onScheduleTap!(TimeScheduleType.start, key);
                    },
                    onEndTap: () {
                      if (onScheduleTap == null) {
                        return;
                      }

                      onScheduleTap!(TimeScheduleType.end, key);
                    },
                    onCloseTap: () {
                      controller.removeItem(key);
                    },
                    startTime: null,
                    endTime: null,
                  ),
                );
              }
            }
          } else {
            for (final key in WeekdayType.values) {
              if (value.map.keys.contains(key)) {
                final item = value.map[key];

                children.add(
                  ScheduleSubItemWidget(
                    startTime: item?.startDateTime == null ? null : item!.startDateTime!.format(context),
                    endTime: item?.endDateTime == null ? null : item!.endDateTime!.format(context),
                    label: _weekdayToString(key),
                  ),
                );
              } else {
                children.add(
                  ScheduleSubItemWidget.empty(
                    label: _weekdayToString(key),
                  ),
                );
              }
            }
          }

          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                label,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: textTheme.headlineLarge!.copyWith(
                  color: textSecondaryColors.secondary100,
                ),
              ),
              const SizedBox(
                height: Spacing.spacing2XS,
              ),
              DecoratedBox(
                decoration: BoxDecoration(
                  color: backgroundColors.gray,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(
                      Radiuses.radius2XS,
                    ),
                  ),
                  border: Border.all(
                    color: dividerPrimaryColors.primary80,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(
                    Spacing.spacing2XS,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: children,
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
