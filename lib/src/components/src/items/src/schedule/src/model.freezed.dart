// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ScheduleSubItemModel {
  TimeOfDay? get startDateTime => throw _privateConstructorUsedError;
  TimeOfDay? get endDateTime => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ScheduleSubItemModelCopyWith<ScheduleSubItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ScheduleSubItemModelCopyWith<$Res> {
  factory $ScheduleSubItemModelCopyWith(ScheduleSubItemModel value,
          $Res Function(ScheduleSubItemModel) then) =
      _$ScheduleSubItemModelCopyWithImpl<$Res, ScheduleSubItemModel>;
  @useResult
  $Res call({TimeOfDay? startDateTime, TimeOfDay? endDateTime});
}

/// @nodoc
class _$ScheduleSubItemModelCopyWithImpl<$Res,
        $Val extends ScheduleSubItemModel>
    implements $ScheduleSubItemModelCopyWith<$Res> {
  _$ScheduleSubItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? startDateTime = freezed,
    Object? endDateTime = freezed,
  }) {
    return _then(_value.copyWith(
      startDateTime: freezed == startDateTime
          ? _value.startDateTime
          : startDateTime // ignore: cast_nullable_to_non_nullable
              as TimeOfDay?,
      endDateTime: freezed == endDateTime
          ? _value.endDateTime
          : endDateTime // ignore: cast_nullable_to_non_nullable
              as TimeOfDay?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ScheduleSubItemModelImplCopyWith<$Res>
    implements $ScheduleSubItemModelCopyWith<$Res> {
  factory _$$ScheduleSubItemModelImplCopyWith(_$ScheduleSubItemModelImpl value,
          $Res Function(_$ScheduleSubItemModelImpl) then) =
      __$$ScheduleSubItemModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({TimeOfDay? startDateTime, TimeOfDay? endDateTime});
}

/// @nodoc
class __$$ScheduleSubItemModelImplCopyWithImpl<$Res>
    extends _$ScheduleSubItemModelCopyWithImpl<$Res, _$ScheduleSubItemModelImpl>
    implements _$$ScheduleSubItemModelImplCopyWith<$Res> {
  __$$ScheduleSubItemModelImplCopyWithImpl(_$ScheduleSubItemModelImpl _value,
      $Res Function(_$ScheduleSubItemModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? startDateTime = freezed,
    Object? endDateTime = freezed,
  }) {
    return _then(_$ScheduleSubItemModelImpl(
      startDateTime: freezed == startDateTime
          ? _value.startDateTime
          : startDateTime // ignore: cast_nullable_to_non_nullable
              as TimeOfDay?,
      endDateTime: freezed == endDateTime
          ? _value.endDateTime
          : endDateTime // ignore: cast_nullable_to_non_nullable
              as TimeOfDay?,
    ));
  }
}

/// @nodoc

class _$ScheduleSubItemModelImpl implements _ScheduleSubItemModel {
  const _$ScheduleSubItemModelImpl({this.startDateTime, this.endDateTime});

  @override
  final TimeOfDay? startDateTime;
  @override
  final TimeOfDay? endDateTime;

  @override
  String toString() {
    return 'ScheduleSubItemModel(startDateTime: $startDateTime, endDateTime: $endDateTime)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ScheduleSubItemModelImpl &&
            (identical(other.startDateTime, startDateTime) ||
                other.startDateTime == startDateTime) &&
            (identical(other.endDateTime, endDateTime) ||
                other.endDateTime == endDateTime));
  }

  @override
  int get hashCode => Object.hash(runtimeType, startDateTime, endDateTime);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ScheduleSubItemModelImplCopyWith<_$ScheduleSubItemModelImpl>
      get copyWith =>
          __$$ScheduleSubItemModelImplCopyWithImpl<_$ScheduleSubItemModelImpl>(
              this, _$identity);
}

abstract class _ScheduleSubItemModel implements ScheduleSubItemModel {
  const factory _ScheduleSubItemModel(
      {final TimeOfDay? startDateTime,
      final TimeOfDay? endDateTime}) = _$ScheduleSubItemModelImpl;

  @override
  TimeOfDay? get startDateTime;
  @override
  TimeOfDay? get endDateTime;
  @override
  @JsonKey(ignore: true)
  _$$ScheduleSubItemModelImplCopyWith<_$ScheduleSubItemModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ScheduleItemModel {
  Map<WeekdayType, ScheduleSubItemModel> get map =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ScheduleItemModelCopyWith<ScheduleItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ScheduleItemModelCopyWith<$Res> {
  factory $ScheduleItemModelCopyWith(
          ScheduleItemModel value, $Res Function(ScheduleItemModel) then) =
      _$ScheduleItemModelCopyWithImpl<$Res, ScheduleItemModel>;
  @useResult
  $Res call({Map<WeekdayType, ScheduleSubItemModel> map});
}

/// @nodoc
class _$ScheduleItemModelCopyWithImpl<$Res, $Val extends ScheduleItemModel>
    implements $ScheduleItemModelCopyWith<$Res> {
  _$ScheduleItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? map = null,
  }) {
    return _then(_value.copyWith(
      map: null == map
          ? _value.map
          : map // ignore: cast_nullable_to_non_nullable
              as Map<WeekdayType, ScheduleSubItemModel>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ScheduleItemModelImplCopyWith<$Res>
    implements $ScheduleItemModelCopyWith<$Res> {
  factory _$$ScheduleItemModelImplCopyWith(_$ScheduleItemModelImpl value,
          $Res Function(_$ScheduleItemModelImpl) then) =
      __$$ScheduleItemModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({Map<WeekdayType, ScheduleSubItemModel> map});
}

/// @nodoc
class __$$ScheduleItemModelImplCopyWithImpl<$Res>
    extends _$ScheduleItemModelCopyWithImpl<$Res, _$ScheduleItemModelImpl>
    implements _$$ScheduleItemModelImplCopyWith<$Res> {
  __$$ScheduleItemModelImplCopyWithImpl(_$ScheduleItemModelImpl _value,
      $Res Function(_$ScheduleItemModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? map = null,
  }) {
    return _then(_$ScheduleItemModelImpl(
      map: null == map
          ? _value._map
          : map // ignore: cast_nullable_to_non_nullable
              as Map<WeekdayType, ScheduleSubItemModel>,
    ));
  }
}

/// @nodoc

class _$ScheduleItemModelImpl implements _ScheduleItemModel {
  const _$ScheduleItemModelImpl(
      {required final Map<WeekdayType, ScheduleSubItemModel> map})
      : _map = map;

  final Map<WeekdayType, ScheduleSubItemModel> _map;
  @override
  Map<WeekdayType, ScheduleSubItemModel> get map {
    if (_map is EqualUnmodifiableMapView) return _map;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_map);
  }

  @override
  String toString() {
    return 'ScheduleItemModel(map: $map)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ScheduleItemModelImpl &&
            const DeepCollectionEquality().equals(other._map, _map));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_map));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ScheduleItemModelImplCopyWith<_$ScheduleItemModelImpl> get copyWith =>
      __$$ScheduleItemModelImplCopyWithImpl<_$ScheduleItemModelImpl>(
          this, _$identity);
}

abstract class _ScheduleItemModel implements ScheduleItemModel {
  const factory _ScheduleItemModel(
          {required final Map<WeekdayType, ScheduleSubItemModel> map}) =
      _$ScheduleItemModelImpl;

  @override
  Map<WeekdayType, ScheduleSubItemModel> get map;
  @override
  @JsonKey(ignore: true)
  _$$ScheduleItemModelImplCopyWith<_$ScheduleItemModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
