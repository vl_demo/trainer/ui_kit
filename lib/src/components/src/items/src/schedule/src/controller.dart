part of '../index.dart';

class ScheduleItemController extends ValueNotifier<ScheduleItemModel> {
  ScheduleItemController({
    required ScheduleItemModel model,
  }) : super(
          model,
        );

  ScheduleItemController.empty()
      : super(
          const ScheduleItemModel(map: {}),
        );

  set likes(String v) {
    //value = value.copyWith(name: v);
  }

  void setTime(
    TimeScheduleType timeScheduleType,
    WeekdayType weekdayType,
    TimeOfDay timeOfDay,
  ) {
    final map = Map<WeekdayType, ScheduleSubItemModel>.from(value.map);

    if (map[weekdayType] != null) {
      map[weekdayType] = map[weekdayType]!.copyWith(
        startDateTime: timeScheduleType == TimeScheduleType.start ? timeOfDay : map[weekdayType]!.startDateTime,
        endDateTime: timeScheduleType == TimeScheduleType.end ? timeOfDay : map[weekdayType]!.endDateTime,
      );
    } else {
      map[weekdayType] = ScheduleSubItemModel(
        startDateTime: timeScheduleType == TimeScheduleType.start ? timeOfDay : null,
        endDateTime: timeScheduleType == TimeScheduleType.end ? timeOfDay : null,
      );
    }

    value = value.copyWith(
      map: map,
    );
  }

  void removeItem(
    WeekdayType weekdayType,
  ) {
    final map = Map<WeekdayType, ScheduleSubItemModel>.from(value.map);

    map.remove(weekdayType);

    value = value.copyWith(
      map: map,
    );
  }
}
