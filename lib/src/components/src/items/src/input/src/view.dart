part of '../index.dart';

class InputItemWidget extends StatelessWidget {
  InputItemWidget({
    super.key,
    required this.label,
    this.isSecure = false,
    this.errorText,
    TextEditingController? textEditingController,
  }) : textEditingController = textEditingController ?? TextEditingController();

  final String label;
  final String? errorText;
  final bool isSecure;

  final TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;

    return Padding(
      padding: const EdgeInsets.all(
        Spacing.spacing2XS,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: textTheme.headlineLarge!.copyWith(
              color: textSecondaryColors.secondary100,
            ),
          ),
          const SizedBox(
            height: Spacing.spacing2XS,
          ),
          TextEditFormField(
            errorText: errorText,
            isSecure: isSecure,
            isVisible: false,
            controller: textEditingController,
          ),
        ],
      ),
    );
  }
}
