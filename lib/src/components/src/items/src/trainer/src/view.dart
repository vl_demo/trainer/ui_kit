part of '../index.dart';

class TrainerItemWidget extends StatelessWidget {
  const TrainerItemWidget({
    super.key,
    required this.imagePath,
    required this.name,
    this.onTap,
    this.imageLoadType = ImageLoadType.network,
  });

  final String imagePath;
  final String name;
  final GestureTapCallback? onTap;
  final ImageLoadType imageLoadType;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;

    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    final Color textColor = textSecondaryColors.secondary100;

    final Color backgroundColor = backgroundColors.gray;

    return DecoratedBox(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(
            Radiuses.radius2XS,
          ),
        ),
        border: Border.all(
          color: dividerSecondaryColors.secondary30,
        ),
      ),
      child: GestureDetector(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(
            Spacing.spacing2XS,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ImageLoad.round(
                fit: BoxFit.cover,
                path: imagePath,
                radius: Radiuses.radius2XL,
                imageLoadType: imageLoadType,
              ),
              const SizedBox(
                width: Spacing.spacing2XS,
              ),
              Text(
                name,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: textTheme.headlineLarge!.copyWith(
                  color: textColor,
                ),
              ),
              const Spacer(),
              const SizedBox(
                width: Spacing.spacing2XS,
              ),
              IconWidget.sizeS(
                color: iconSecondaryColors.secondary70,
                type: IconType.arrowForward,
              )
            ],
          ),
        ),
      ),
    );
  }
}
