part of '../index.dart';

class FillButtonWidget extends StatelessWidget {
  const FillButtonWidget({
    super.key,
    required this.label,
    this.onPressed,
  });

  final String label;

  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final buttonPrimaryColors = theme.extension<ButtonPrimaryColors>()!;

    return TextButton(
      onPressed: onPressed,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: buttonPrimaryColors.defaultFill,
          borderRadius: const BorderRadius.all(
            Radius.circular(
              Radiuses.radius2XS,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: Spacing.spacingS,
            vertical: Spacing.spacingXS,
          ),
          child: Text(
            label,
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            style: textTheme.headlineLarge!.copyWith(
              color: buttonPrimaryColors.defaultText,
            ),
          ),
        ),
      ),
    );
  }
}
