part of '../index.dart';

class ClearButtonWidget extends StatelessWidget {
  const ClearButtonWidget({
    super.key,
    required this.label,
    this.onPressed,
  });

  final String label;

  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textPrimaryColors = theme.extension<TextPrimaryColors>()!;

    return TextButton(
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Spacing.spacingS,
          vertical: Spacing.spacingXS,
        ),
        child: Text(
          label,
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
          style: textTheme.bodyLarge!.copyWith(
            color: textPrimaryColors.primary80,
          ),
        ),
      ),
    );
  }
}
