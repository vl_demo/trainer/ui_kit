import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:ui_kit/ui_kit.dart';

class ImageLoad extends StatelessWidget {
  const ImageLoad({
    this.path,
    this.image,
    this.width,
    this.height,
    this.fit = BoxFit.contain,
    this.imageLoadType = ImageLoadType.network,
    this.blurhash,
    super.key,
  })  : isCircle = false,
        radius = null;

  const ImageLoad.round({
    this.path,
    this.image,
    this.fit = BoxFit.contain,
    this.imageLoadType = ImageLoadType.network,
    this.blurhash,
    super.key,
    required this.radius,
  })  : isCircle = true,
        width = null,
        height = null;

  final String? path;
  final Uint8List? image;
  final double? width;
  final double? height;
  final BoxFit fit;
  final String? blurhash;
  final ImageLoadType imageLoadType;
  final double? radius;
  final bool isCircle;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final badgeBadColors = theme.extension<BadgeBadColors>()!;

    final Widget imageWidget;

    switch (imageLoadType) {
      case ImageLoadType.asset:
        imageWidget = Image.asset(
          path ?? '',
          width: width,
          height: height,
          fit: fit,
        );
      case ImageLoadType.file:
        imageWidget = Image.asset(
          path ?? '',
          width: width,
          height: height,
          fit: fit,
        );
      case ImageLoadType.network:
        final String? sessionToken = SessionInheritedWidget.of(context)?.sessionToken;
        final httpHeaders = <String, String>{};

        if (sessionToken != null) {
          httpHeaders.addAll({'Authorization': 'Bearer $sessionToken'});
        }

        imageWidget = CachedNetworkImage(
          httpHeaders: httpHeaders,
          imageUrl: path ?? '',
          width: width,
          height: height,
          fit: fit,
          placeholder: (
            BuildContext context,
            String url,
          ) {
            return const Center(
              child: LoaderIndicator.sm(),
            );
          },
          imageBuilder: (context, imageProvider) {
            return DecoratedBox(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: imageProvider,
                  fit: fit,
                  //colorFilter: ColorFilter.mode(Colors.red, BlendMode.colorBurn),
                ),
              ),
            );
          },
          errorWidget: (
            BuildContext context,
            String url,
            Object error,
          ) {
            return SizedBox(
              width: width,
              height: height,
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: badgeBadColors.filledFill,
                ),
                child: Center(
                  child: IconWidget.size2XM(
                    color: badgeBadColors.strokedFill,
                    type: IconType.remove,
                  ),
                ),
              ),
            );
          },
        );

      case ImageLoadType.memory:
        imageWidget = Image.memory(
          image ?? Uint8List(0),
          width: width,
          height: height,
          fit: fit,
        );
    }

    if (radius != null) {
      return SizedBox(
        width: radius,
        height: radius,
        child: ClipRRect(
          borderRadius: isCircle
              ? BorderRadius.circular(
                  radius! * 2,
                )
              : BorderRadius.all(
                  Radius.circular(
                    radius!,
                  ),
                ),
          child: imageWidget,
        ),
      );
    }

    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: imageWidget,
    );
  }
}
