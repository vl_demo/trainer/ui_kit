part of '../../index.dart';

class FontSizes {
  static const fontSize3XL = 36.0;
  static const fontSize2XL = 32.0;
  static const fontSizeXL = 28.0;
  static const fontSizeL = 24.0;
  static const fontSizeS = 18.0;
  static const fontSizeXS = 16.0;
  static const fontSize2XS = 15.0;
  static const fontSize3XS = 14.0;
  static const fontSize4XS = 12.0;
}

class LineHeights {
  static const lineHeight2XL = 56.0;
  static const lineHeightL = 36.0;
  static const lineHeightS = 28.0;
  static const lineHeightXS = 24.0;
  static const lineHeight2XS = 20.0;
  static const lineHeight3XS = 16.0;
}
