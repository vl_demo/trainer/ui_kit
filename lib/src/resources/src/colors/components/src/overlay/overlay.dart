part of '../../index.dart';

///
/// Где использовать
///
/// Средний информационный слой между Background и Surface.
/// Некликабельная область заднего плана интерфейса.
///
class OverlayColors extends ThemeExtension<OverlayColors> {
  OverlayColors({
    required this.black,
    required this.gray,
  });

  final Color black;
  final Color gray;

  @override
  OverlayColors lerp(OverlayColors? other, double t) {
    if (other is! OverlayColors) {
      return this;
    }

    return OverlayColors(
      black: Color.lerp(black, other.black, t)!,
      gray: Color.lerp(gray, other.gray, t)!,
    );
  }

  @override
  OverlayColors copyWith({
    Color? black,
    Color? gray,
  }) {
    return OverlayColors(
      black: black ?? this.black,
      gray: gray ?? this.gray,
    );
  }
}
