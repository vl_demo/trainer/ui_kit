part of '../../index.dart';

class ProgressGoodColors extends ThemeExtension<ProgressGoodColors> {
  ProgressGoodColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressGoodColors lerp(ProgressGoodColors? other, double t) {
    if (other is! ProgressGoodColors) {
      return this;
    }

    return ProgressGoodColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressGoodColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressGoodColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
