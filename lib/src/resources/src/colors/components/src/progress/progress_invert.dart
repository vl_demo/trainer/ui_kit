part of '../../index.dart';

class ProgressInvertColors extends ThemeExtension<ProgressInvertColors> {
  ProgressInvertColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressInvertColors lerp(ProgressInvertColors? other, double t) {
    if (other is! ProgressInvertColors) {
      return this;
    }

    return ProgressInvertColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressInvertColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressInvertColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
