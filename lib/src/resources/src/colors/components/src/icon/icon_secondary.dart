part of '../../index.dart';

class IconSecondaryColors extends ThemeExtension<IconSecondaryColors> {
  IconSecondaryColors({
    required this.secondary100,
    required this.secondary70,
    required this.secondary40,
    required this.secondary20,
    required this.secondary0,
  });

  final Color secondary100;
  final Color secondary70;
  final Color secondary40;
  final Color secondary20;
  final Color secondary0;

  @override
  IconSecondaryColors lerp(IconSecondaryColors? other, double t) {
    if (other is! IconSecondaryColors) {
      return this;
    }

    return IconSecondaryColors(
      secondary100: Color.lerp(secondary100, other.secondary100, t)!,
      secondary70: Color.lerp(secondary70, other.secondary70, t)!,
      secondary40: Color.lerp(secondary40, other.secondary40, t)!,
      secondary20: Color.lerp(secondary20, other.secondary20, t)!,
      secondary0: Color.lerp(secondary0, other.secondary0, t)!,
    );
  }

  @override
  IconSecondaryColors copyWith({
    Color? secondary100,
    Color? secondary70,
    Color? secondary50,
    Color? secondary30,
    Color? secondary0,
  }) {
    return IconSecondaryColors(
      secondary100: secondary100 ?? this.secondary100,
      secondary70: secondary70 ?? this.secondary70,
      secondary40: secondary50 ?? this.secondary40,
      secondary20: secondary30 ?? this.secondary20,
      secondary0: secondary0 ?? this.secondary0,
    );
  }
}
