part of '../../index.dart';

class IconSystemColors extends ThemeExtension<IconSystemColors> {
  IconSystemColors({
    required this.good,
    required this.bad,
    required this.warning,
    required this.info,
    required this.attention,
  });

  final Color good;
  final Color bad;
  final Color warning;
  final Color info;
  final Color attention;

  @override
  IconSystemColors lerp(IconSystemColors? other, double t) {
    if (other is! IconSystemColors) {
      return this;
    }

    return IconSystemColors(
      good: Color.lerp(good, other.good, t)!,
      bad: Color.lerp(bad, other.bad, t)!,
      warning: Color.lerp(warning, other.warning, t)!,
      info: Color.lerp(info, other.info, t)!,
      attention: Color.lerp(attention, other.attention, t)!,
    );
  }

  @override
  IconSystemColors copyWith({
    Color? good,
    Color? bad,
    Color? warning,
    Color? info,
    Color? attention,
  }) {
    return IconSystemColors(
      good: good ?? this.good,
      bad: bad ?? this.bad,
      warning: warning ?? this.warning,
      info: info ?? this.info,
      attention: attention ?? this.attention,
    );
  }
}
