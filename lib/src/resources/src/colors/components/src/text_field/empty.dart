part of '../../index.dart';

class TextFieldEmptyColors extends ThemeExtension<TextFieldEmptyColors> {
  TextFieldEmptyColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.activatedBorder,
    required this.focusedBorder2,
    required this.defaultText,
    required this.hoverText,
    required this.disabledText,
    required this.focusedText,
    required this.activatedText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.disabledIcon,
    required this.focusedIcon,
    required this.activatedIcon,
    required this.counterDefaultText,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color activatedBorder;
  final Color focusedBorder2;
  final Color defaultText;
  final Color hoverText;
  final Color disabledText;
  final Color focusedText;
  final Color activatedText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color disabledIcon;
  final Color focusedIcon;
  final Color activatedIcon;
  final Color counterDefaultText;

  @override
  TextFieldEmptyColors lerp(TextFieldEmptyColors? other, double t) {
    if (other is! TextFieldEmptyColors) {
      return this;
    }

    return TextFieldEmptyColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      activatedBorder: Color.lerp(activatedBorder, other.activatedBorder, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      disabledText: Color.lerp(disabledText, other.disabledText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
      activatedText: Color.lerp(activatedText, other.activatedText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      disabledIcon: Color.lerp(disabledIcon, other.disabledIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
      activatedIcon: Color.lerp(activatedIcon, other.activatedIcon, t)!,
      counterDefaultText:
          Color.lerp(counterDefaultText, other.counterDefaultText, t)!,
    );
  }

  @override
  TextFieldEmptyColors copyWith({
    Color? filledFill,
    Color? defaultFill,
    Color? hoverFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? activatedBorder,
    Color? focusedBorder2,
    Color? defaultText,
    Color? hoverText,
    Color? disabledText,
    Color? focusedText,
    Color? activatedText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? disabledIcon,
    Color? focusedIcon,
    Color? activatedIcon,
    Color? counterDefaultText,
  }) {
    return TextFieldEmptyColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      activatedBorder: activatedBorder ?? this.activatedBorder,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      disabledText: disabledText ?? this.disabledText,
      focusedText: focusedText ?? this.focusedText,
      activatedText: activatedText ?? this.activatedText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      disabledIcon: disabledIcon ?? this.disabledIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
      activatedIcon: activatedIcon ?? this.activatedIcon,
      counterDefaultText: counterDefaultText ?? this.counterDefaultText,
    );
  }
}
