part of '../../index.dart';

class TextPrimaryColors extends ThemeExtension<TextPrimaryColors> {
  TextPrimaryColors({
    required this.primary80,
    required this.primary60,
    required this.primary40,
  });

  final Color primary80;
  final Color primary60;
  final Color primary40;

  @override
  TextPrimaryColors lerp(TextPrimaryColors? other, double t) {
    if (other is! TextPrimaryColors) {
      return this;
    }

    return TextPrimaryColors(
      primary80: Color.lerp(primary80, other.primary80, t)!,
      primary60: Color.lerp(primary60, other.primary60, t)!,
      primary40: Color.lerp(primary40, other.primary40, t)!,
    );
  }

  @override
  TextPrimaryColors copyWith({
    Color? primary80,
    Color? primary60,
    Color? primary40,
  }) {
    return TextPrimaryColors(
      primary80: primary80 ?? this.primary80,
      primary60: primary60 ?? this.primary60,
      primary40: primary40 ?? this.primary40,
    );
  }
}
