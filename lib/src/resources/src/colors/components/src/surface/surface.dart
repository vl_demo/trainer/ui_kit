part of '../../index.dart';

///
/// Где использовать
/// Второй информационный слой, находящиеся уровнем выше над Background
///
/// * Header
/// * Footer
/// * NavBar
/// * Sidebar
/// * Menu
/// * Message (включая Comments)
/// * Modal (включая подложку под DropdownList S)
///
class SurfaceColors extends ThemeExtension<SurfaceColors> {
  SurfaceColors({
    required this.white,
    required this.gray,
    required this.darkGray,
    required this.error,
    required this.gradientBlue,
    required this.gradientOrange,
    required this.gradientGreen,
    required this.gradientWhiteUp,
    required this.gradientWhiteDown,
  });

  final Color white;
  final Color gray;
  final Color darkGray;
  final Color error;
  final Gradient gradientBlue;
  final Gradient gradientOrange;
  final Gradient gradientGreen;
  final Gradient gradientWhiteUp;
  final Gradient gradientWhiteDown;

  @override
  SurfaceColors lerp(SurfaceColors? other, double t) {
    if (other is! SurfaceColors) {
      return this;
    }

    return SurfaceColors(
      white: Color.lerp(white, other.white, t)!,
      gray: Color.lerp(gray, other.gray, t)!,
      darkGray: Color.lerp(darkGray, other.darkGray, t)!,
      error: Color.lerp(error, other.error, t)!,
      gradientBlue: Gradient.lerp(gradientBlue, other.gradientBlue, t)!,
      gradientOrange: Gradient.lerp(gradientOrange, other.gradientOrange, t)!,
      gradientGreen: Gradient.lerp(gradientGreen, other.gradientGreen, t)!,
      gradientWhiteUp:
          Gradient.lerp(gradientWhiteUp, other.gradientWhiteUp, t)!,
      gradientWhiteDown:
          Gradient.lerp(gradientWhiteDown, other.gradientWhiteDown, t)!,
    );
  }

  @override
  SurfaceColors copyWith({
    Color? white,
    Color? gray,
    Color? darkGray,
    Color? error,
    Gradient? gradientBlue,
    Gradient? gradientOrange,
    Gradient? gradientGreen,
    Gradient? gradientWhiteUp,
    Gradient? gradientWhiteDown,
  }) {
    return SurfaceColors(
      white: white ?? this.white,
      gray: gray ?? this.gray,
      darkGray: gray ?? this.darkGray,
      error: error ?? this.error,
      gradientBlue: gradientBlue ?? this.gradientBlue,
      gradientOrange: gradientOrange ?? this.gradientOrange,
      gradientGreen: gradientGreen ?? this.gradientGreen,
      gradientWhiteUp: gradientWhiteUp ?? this.gradientWhiteUp,
      gradientWhiteDown: gradientWhiteDown ?? this.gradientWhiteDown,
    );
  }
}
