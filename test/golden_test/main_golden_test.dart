import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:ui_kit/generated/assets.dart';
import 'package:ui_kit/ui_kit.dart';

const _surfaceWidth = 800.0;

void main() async {
  await loadAppFonts();

  const defaultTheme = 'defaultTheme';

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'ScheduleItemWidget()',
            _GoldenTestWrapperWidget(
              child: ScheduleItemWidget(
                sunLabel: 'Sun',
                monLabel: 'Mon',
                tueLabel: 'Tue',
                wedLabel: 'Wed',
                thuLabel: 'Thu',
                friLabel: 'Fri',
                satLabel: 'Sat',
                label: 'Schedule',
                model: const ScheduleItemModel(
                  map: {
                    WeekdayType.sun: ScheduleSubItemModel(
                      startDateTime: TimeOfDay(
                        hour: 10,
                        minute: 30,
                      ),
                      endDateTime: TimeOfDay(
                        hour: 14,
                        minute: 30,
                      ),
                    ),
                    WeekdayType.thu: ScheduleSubItemModel(
                      startDateTime: TimeOfDay(
                        hour: 10,
                        minute: 30,
                      ),
                    ),
                    WeekdayType.wed: ScheduleSubItemModel(
                      endDateTime: TimeOfDay(
                        hour: 14,
                        minute: 30,
                      ),
                    )
                  },
                ),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}ScheduleItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'InputItemWidget()',
            _GoldenTestWrapperWidget(
              child: InputMultilineItemWidget(
                label: 'Foobar',
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}InputItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'LabelItemWidget()',
            const _GoldenTestWrapperWidget(
              child: LabelItemWidget(
                label: "FooBar",
                value: "FooBar",
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}LabelItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'TrainerItemWidget()',
            _GoldenTestWrapperWidget(
              child: TrainerItemWidget(
                imagePath: Assets.imageAvatar1.packagePrefix,
                name: "FooBar",
                imageLoadType: ImageLoadType.asset,
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}TrainerItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'CalendarItemWidget()',
            const _GoldenTestWrapperWidget(
              child: CalendarItemWidget(
                label: 'Birthday',
                value: "13.05.1980",
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}CalendarItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'buttons/',
    () {
      const path = '$defaultTheme/buttons/';

      testGoldens('buttons', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'FillButtonWidget()',
            const _GoldenTestWrapperWidget(
              child: FillButtonWidget(
                label: "FooBar",
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}FillButtonWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'buttons/',
    () {
      const path = '$defaultTheme/buttons/';

      testGoldens('buttons', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'ClearButtonWidget()',
            const _GoldenTestWrapperWidget(
              child: ClearButtonWidget(
                label: "FooBar",
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}ClearButtonWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'search_bars/default/',
    () {
      const path = '$defaultTheme/search_bars/default/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'SearchBarWidget()',
            _GoldenTestWrapperWidget(
              child: SearchBarWidget(
                cancelLabel: 'Cancel',
                model: const SearchBarModel(text: ''),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.lightTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}SearchBarWidget',
          autoHeight: true,
        );
      });
    },
  );
}

class _GoldenTestWrapperWidget extends StatelessWidget {
  const _GoldenTestWrapperWidget({
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Center(
        child: child,
      ),
    );
  }
}
